<?php
/**
 * @file
 * project_issue.field_group.inc
 */

/**
 * Implementation of hook_field_group_info().
 */
function project_issue_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->api_version = 1;
  $field_group->identifier = 'group_issue_information|node|issue|form';
  $field_group->group_name = 'group_issue_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'issue';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Issue Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_issue_category',
      1 => 'field_issue_priority',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_issue_information|node|issue|form'] = $field_group;

  return $export;
}
